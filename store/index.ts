import { configureStore } from '@reduxjs/toolkit';
import gameReducer from './features/game/gameSlice';


const store = configureStore({
  reducer: {
    game: gameReducer,
  },
});

export type GameState = ReturnType<typeof store.getState>;
export type GameDispatch = typeof store.dispatch;
export default store;