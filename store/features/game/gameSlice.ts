import { createSlice } from '@reduxjs/toolkit';

interface GameState {
  notifCount: number,
  isAnimating: boolean,
}

const initialState: GameState = {
  notifCount: 0,
  isAnimating: false,
};

export const gameSlice = createSlice({
  name: 'game',
  initialState,
  reducers: {
    notifyRelatives: (state) => {
      const canvas = document.getElementById('game_canvas') as HTMLCanvasElement;
      const firstNode = document.getElementById('origin') as HTMLDivElement;
      canvas.width = firstNode.scrollWidth;
      canvas.height = firstNode.scrollHeight;
      const ctx = canvas.getContext('2d');
      if (ctx) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      }
      state.notifCount += 1;
    },
    startAnimation: (state) => {
      state.isAnimating = true;
    },
    endAnimation: (state) => {
      state.isAnimating = false;
    },
  },
});

export const {
  notifyRelatives,
  startAnimation,
  endAnimation,
} = gameSlice.actions;

export default gameSlice.reducer;