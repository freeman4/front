const path = require('path');

// module.exports = {
//   webpack: (
//     config,
//     { buildId, dev, isServer, defaultLoaders, webpack}
//   ) => {

//     return config
//   },
// }


module.exports = {
  async rewrites() {
    return [
      {
        source: '/tmdb/:path',
        destination: 'https://localhost:3001/:path',
      }
    ]
  }
}