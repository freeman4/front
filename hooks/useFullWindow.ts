import { RefObject, useEffect, useState } from 'react'
import getRootComputedStyle, { VarType } from '../libs/styles/getRootComputedStyle';

const useFullWindow = (ref: RefObject<HTMLDivElement>) => {
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);

  useEffect(() => {
    setWidth(document.documentElement.clientWidth);
    setHeight(document.documentElement.clientHeight);

    const handleResize = () => {
      setWidth(document.documentElement.clientWidth);
      setHeight(document.documentElement.clientHeight);
    }

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    }
  }, []);

  const div = ref.current;
  if (div) {
    div.style.minWidth = `${width}px`;
    div.style.minHeight = `${height}px`;
  }
}

export default useFullWindow;