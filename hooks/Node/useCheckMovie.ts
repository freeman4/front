import { MovieCheckDto } from '../../common/dto/tmbProxy/tmdb-proxy-movie.dto';
import { checkMovie } from '../swrFetchers/searchTmdb';
import { useMutation } from 'react-query';
import { useEffect, useState } from 'react';


const useCheckMovie = (
  text: string,
  isActor: boolean,
  actorId: number
) => {
  const movieCheck = useMutation((movieTxt: string) => checkMovie(movieTxt, actorId));

  useEffect(() => {
    if (text && isActor) {
      movieCheck.mutateAsync(text);
    }
  }, [text]);

  return movieCheck;
}

export default useCheckMovie;