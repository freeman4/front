import { useEffect, useState } from 'react';
import { useMutation } from 'react-query';
import { ActorCheckDto } from '../../common/dto/tmbProxy/tmdb-proxy-actor.dto';
import { checkActor } from '../swrFetchers/searchTmdb';


const useCheckActor = (
  text: string,
  isActor: boolean,
  movieId: number,
) => {
  const actorCheck = useMutation((actorTxt: string) => checkActor(actorTxt, movieId));

  useEffect(() => {
    if (text && !isActor) {
      actorCheck.mutateAsync(text);
    }
  }, [text]);

  return actorCheck;
}

export default useCheckActor;