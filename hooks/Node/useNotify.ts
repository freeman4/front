import { useEffect } from 'react';
import { notifyRelatives, startAnimation } from '../../store/features/game/gameSlice';
import { useGameDispatch } from '../../store/hooks';



const useNotify = (isN0: boolean) => {
  const dispatch = useGameDispatch();

  useEffect(() => {
    dispatch(notifyRelatives());
    // no connection animation for the first node
    if (!isN0) dispatch(startAnimation());
    return () => {
      dispatch(notifyRelatives());
    }
  }, []);
}

export default useNotify;