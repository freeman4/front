import { useEffect, useRef, useState } from 'react';
import { ActorResultDto } from '../../common/dto/tmdb/tmdb-actor.dto';
import { MovieResultDto } from '../../common/dto/tmdb/tmdb-movie.dto';
import FreemanNode from '../../components/FreemanNode';
import IFreemanData from '../../models/Node/IFreemanData';

const createNode = (
  isActor: boolean,
  data: MovieResultDto | ActorResultDto,
  deleteChildren: (nodeKey: string) => void,
) => {
  const movieData = isActor ? (data as MovieResultDto) : undefined;
  const actorData = !isActor ? (data as ActorResultDto) : undefined;

  return (
    <FreemanNode
      key={crypto.randomUUID()}
      isActor={!isActor}
      nodeKey={crypto.randomUUID()}
      movieData={movieData}
      actorData={actorData}
      parentDelChild={deleteChildren}
    />
  );
}

/**
 * generates FreemanNode's childrens with an API call
 * @returns an object to manipulate childs FreemanNodes
 */
const useChildrens = (isActor: boolean) => {
  const [childrens, setChildrens] = useState<JSX.Element[]>([]);

  const deleteChildren = (nodeKey: string) => {
    setChildrens(prev => prev.filter(c => c.props.nodeKey !== nodeKey));
  };

  const addChildren = (data: MovieResultDto | ActorResultDto) => {
    setChildrens([ ...childrens, createNode(isActor, data, deleteChildren) ]);
  };

  return {
    childrens,
    addChildren,
  };
}

export default useChildrens;