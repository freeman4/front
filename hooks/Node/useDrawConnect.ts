import { RefObject, useEffect, useState } from 'react';
import getNorm from '../../libs/maths/getNorm';
import getRadian from '../../libs/maths/getRadian';
import getSegmentMiddle from '../../libs/maths/getSegmentMiddle';
import { endAnimation } from '../../store/features/game/gameSlice';
import { useGameDispatch, useGameSelector } from '../../store/hooks';
// import { nodeGap, nodeHeight } from '../../styles/variables.scss';

const nodeGap = 50;
const nodeHeight = 50;

interface IOffset {
  left: number,
  top: number,
}

const getFullOffset = (
  freemanBloc: HTMLDivElement,
  first = true,
  offset = { left: 0, top: 0 },
): IOffset => {
  const freemanNode = (freemanBloc.offsetParent as HTMLDivElement);
  if (freemanNode.id === 'origin') {

    return {
      left: offset.left + freemanBloc.offsetWidth + freemanBloc.offsetLeft,
      top: offset.top,
    };
  }
  return getFullOffset(
    (freemanNode.parentElement?.parentElement?.children[0] as HTMLDivElement),
    false,
    {
      left: offset.left + freemanBloc.offsetLeft + (first ? 0 : freemanBloc.offsetWidth),
      top: offset.top + freemanNode.offsetTop,
    },
  );
}

const animateArc = async (
  cbOnEnd: () => void,
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  radius: number,
  startAngle: number,
  endAngle: number,
  counterClockWise?: boolean,
) => {
  let animEndAngle = startAngle;
  let animStartAngle = startAngle;
  const step = Math.abs(endAngle - startAngle) / 15;

  while (
    counterClockWise ? 
      animEndAngle > endAngle : animEndAngle < endAngle
  ) {
    if (counterClockWise) {
      animEndAngle = animEndAngle - step > endAngle ?
        animEndAngle - step : endAngle;
    } else {
      animEndAngle = animEndAngle + step < endAngle ?
        animEndAngle + step : endAngle;
    }

    animStartAngle = await new Promise((resolve, reject) => {
      window.requestAnimationFrame(() => {
        ctx.beginPath();
        if (counterClockWise) {
          ctx.arc(x, y, radius, animEndAngle, animStartAngle)
        } else {
          ctx.arc(x, y, radius, animStartAngle, animEndAngle);
        }
        ctx.stroke();
        resolve(animEndAngle);
      });
    });
  }
  cbOnEnd();

  return true;
}

/**
 * draws the visual connection between a node and his parent
 * @param isN0 we don't want to draw if it's the first node
 * @param ref the react reference to the FreemanBloc div
 */
const useDrawConnect = (
  isN0: boolean,
  ref: RefObject<HTMLDivElement>,
) => {
  const [animate, setAnimate] = useState(true);

  const { notifCount, isAnimating } = useGameSelector(state => state.game);
  const dispatch = useGameDispatch();

  useEffect(() => {
    const canvas = document.getElementById('game_canvas') as HTMLCanvasElement;
    const ctx = canvas.getContext('2d');
    const computedStyle = getComputedStyle(document.documentElement);
    const nodeGap = parseInt(computedStyle.getPropertyValue('--node-gap'));
    const nodeHeight = parseInt(computedStyle.getPropertyValue('--node-height'));

    const fBloc = ref.current;
    if (ctx && fBloc && !isN0 && (!animate || (animate && isAnimating))) {
      ctx.globalAlpha = 0.3;
      
      const offsetTop = (fBloc.offsetParent as HTMLDivElement).offsetTop;
      const offsetLeft = fBloc.offsetLeft;
      const fullOffset = getFullOffset(fBloc);

      // right side of parent FreemanNode
      const posParent = {
        x: fullOffset.left - offsetLeft,
        y: fullOffset.top - offsetTop + nodeHeight/2,
      };
      // left side of this FreemanNode
      const pos = {
        x: fullOffset.left,
        y: fullOffset.top + nodeHeight/2,
      }
      const sMid = getSegmentMiddle(posParent, pos);

      if (offsetTop === 0) {
        ctx.beginPath();
        ctx.moveTo(posParent.x, posParent.y);
        ctx.lineTo(pos.x, pos.y);
        ctx.stroke();
        if (animate) {
          dispatch(endAnimation());
          setAnimate(false);
        }
      } else {
        const radian = getRadian(posParent, sMid);
        const adjacent = getNorm(pos, sMid) / 2;
        const radius = adjacent / Math.cos(radian);

        // center of the left arc
        const center1 = {
          x: sMid.x - radius,
          y: sMid.y,
        };
        // center of the right arc
        const center2 = {
          x: sMid.x + radius,
          y: sMid.y,
        };

        const rad = getRadian(center1, posParent);false
        // const rad2 = getRadian(center2, pos);

        if (animate) {
          animateArc(() => {}, ctx, center1.x, center1.y, radius, 2*Math.PI - rad, 2*Math.PI)
            .then(() => {
              animateArc(
                () => {
                  setAnimate(false);
                  dispatch(endAnimation());
                },
                ctx, center2.x,
                center2.y,
                radius,
                Math.PI,
                Math.PI - rad,
                true,
              );
            });
        } else {
          ctx.beginPath();
          ctx.arc(center1.x, center1.y, radius, 2*Math.PI - rad, 2*Math.PI);
          ctx.stroke();

          ctx.beginPath();
          ctx.arc(center2.x, center2.y, radius, Math.PI - rad, Math.PI);
          ctx.stroke();
        }
      }

    }
  }, [ref, notifCount]);

}

export default useDrawConnect;