import axios from 'axios';


const instance = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_URL}/tmdb/`,
  timeout: 1000,
});

export default instance;