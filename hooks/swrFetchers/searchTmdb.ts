import { AxiosResponse } from 'axios';
import { ActorCheckDto } from '../../common/dto/tmbProxy/tmdb-proxy-actor.dto';
import { MovieCheckDto } from '../../common/dto/tmbProxy/tmdb-proxy-movie.dto';
import { ActorResultDto } from '../../common/dto/tmdb/tmdb-actor.dto';
import { MovieResultDto } from '../../common/dto/tmdb/tmdb-movie.dto';
import axiosTmdb from "./axiosTmdb";

export const searchMovies = async (
  search: string,
): Promise<AxiosResponse<MovieResultDto[]>> => {
  return axiosTmdb.get(`movie/search/${search}`);
}

export const searchActors = async (
  search: string,
): Promise<AxiosResponse<ActorResultDto[]>> => {
  return axiosTmdb.get(`actor/search/${search}`);
}

export const checkMovie = async (
  movie: string,
  tmdbActorId: number,
): Promise<AxiosResponse<MovieCheckDto>> => {
  return axiosTmdb.get(`movie`, {
    params: {
      movie,
      tmdb_actor_id: tmdbActorId,
    },
  });
}

export const checkActor = async (
  actor: string,
  tmdbMovieId: number,
): Promise<AxiosResponse<ActorCheckDto>> => {
  return axiosTmdb.get(`actor`, {
    params: {
      actor,
      tmdb_movie_id: tmdbMovieId,
    },
  });
}

export const popularActor = async (
  pages: number,
): Promise<AxiosResponse<ActorResultDto>> => {
  return axiosTmdb.get('actor/popular', {
    params: {
      pages,
    },
  });
}