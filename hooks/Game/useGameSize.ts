import { RefObject, useEffect, useState } from 'react'
import getRootComputedStyle, { VarType } from '../../libs/styles/getRootComputedStyle';

const useGameSize = (ref: RefObject<HTMLDivElement>) => {
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);
  const [footerHeight, setFooterHeight] = useState(0);
  const [navHeight, setNavHeight] = useState(0);

  useEffect(() => {

    const handleResizeAndLoad = () => {
      setWidth(document.documentElement.clientWidth);
      setHeight(document.documentElement.clientHeight);
      setFooterHeight(
        getRootComputedStyle('--footer-height', VarType.Number) as number
      );
      setNavHeight(
        getRootComputedStyle('--nav-height', VarType.Number) as number
      );
    }

    handleResizeAndLoad();

    window.addEventListener('resize', handleResizeAndLoad);

    return () => {
      window.removeEventListener('resize', handleResizeAndLoad);
    }
  }, []);

  const div = ref.current;
  
  if (div) {
    div.style.maxWidth = `${width}px`;
    console.log(footerHeight);
    div.style.maxHeight = `${height - footerHeight - navHeight}px`
  }
}

export default useGameSize;