import { FunctionComponent, KeyboardEvent, useEffect, useRef, useState } from 'react';
import useChildrens from '../hooks/Node/useChildrens';
import IFreemanData from '../models/Node/IFreemanData';
import FreemanBloc from './FreemanBloc';
import styles from '../styles/components/FreemanNode.module.scss';
import fadeTransition from '../styles/animations/fade.module.css';
import { useGameSelector } from '../store/hooks';
import { CSSTransition } from 'react-transition-group';
import { MovieResultDto } from '../common/dto/tmdb/tmdb-movie.dto';
import { ActorResultDto } from '../common/dto/tmdb/tmdb-actor.dto';
import { useMutation } from 'react-query';
import { checkActor, checkMovie } from '../hooks/swrFetchers/searchTmdb';
import useCheckMovie from '../hooks/Node/useCheckMovie';
import useCheckActor from '../hooks/Node/useCheckActor';

interface IProps {
  actorData?: ActorResultDto,
  movieData?: MovieResultDto,
  // data: IFreemanData,
  isActor: boolean,
  parentDelChild?: (key: string) => void,
  nodeKey?: string,
};

const FreemanNode: FunctionComponent<IProps> = ({
  actorData,
  movieData,
  isActor,
  parentDelChild,
  nodeKey,
}) => {
  const [show, setShow] = useState(false);
  const [text, setText] = useState('');
  // const [movie, setMovie] = useState<MovieResultDto>();
  // const [actor, setActor] = useState<ActorResultDto>();

  const isAnimating = useGameSelector(state => state.game.isAnimating);
  const { childrens, addChildren } = useChildrens(isActor);

  const curr = useRef(isAnimating).current;

  const getName = () => {
    if (actorData) return actorData.name;
    if (movieData) return movieData.title;
    return 'nom ou titre introuvable';
  }
  const movieId = movieData ? movieData.id : 0;
  const actorId = actorData ? actorData.id : 0;
  // TODO: add children with data on movie check or actor check
  const movieCheck = useCheckMovie(text, isActor, actorId);
  const actorCheck = useCheckActor(text, isActor, movieId)
  // if (movieCheck)

  const handleInput = (e: KeyboardEvent, text: string) => {
    // cannot add bloc during animation
    if (e.key === 'Enter' && !isAnimating) {
      setText(text);
      // addChildren(text)
    };
  };
  const id = parentDelChild && nodeKey ? '' : 'origin';

  useEffect(() => {
    if (movieCheck.isSuccess && movieCheck.data.data.movie) {
      addChildren(movieCheck.data.data.movie);
    }
    if (actorCheck.isSuccess && actorCheck.data.data.actor) {
      addChildren(actorCheck.data.data.actor);
    }
  }, [movieCheck.isSuccess, actorCheck.isSuccess]);

  useEffect(() => {
    setTimeout(() => setShow(true), 500);
  }, []);

  return <div id={id} className={styles.node}>
    <CSSTransition
      timeout={300}
      // cast needed: component needs class but it isn't defined in module
      classNames={fadeTransition}
      in
      appear
      unmountOnExit
    >
        {
            <FreemanBloc
              movieCheck={movieCheck.data?.data}
              actorCheck={actorCheck.data?.data}
              isActor={isActor}
              name={getName()}
              handleInput={handleInput}
              deleteNode={
                parentDelChild && nodeKey ?
                  () => parentDelChild(nodeKey) : undefined
              }
            />
        }
    </CSSTransition>
    <div>
      {childrens.map(c => c)}
    </div>
  </div>;
};

export default FreemanNode;