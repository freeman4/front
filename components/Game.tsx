import { FunctionComponent, useRef } from 'react';
import FreemanNode from './FreemanNode';
import styles from '../styles/components/Game.module.scss';
import { Provider } from 'react-redux';
import store from '../store';
import useGameSize from '../hooks/Game/useGameSize';
import { QueryClient, QueryClientProvider, useMutation, useQuery } from 'react-query';
import { popularActor } from '../hooks/swrFetchers/searchTmdb';

interface IProps {

}

const Game: FunctionComponent<IProps> = () => {

  const gameRef = useRef<HTMLDivElement>(null);

  useGameSize(gameRef);
  const actorA = useQuery('actorA', () => popularActor(10));
  const actorB = useQuery('actorB', () => popularActor(10));

  if (actorA.isError || actorB.isError) {
    return <div>
      Une erreur est survenue.
    </div>
  }

  if ((!actorA.isSuccess && !actorB.isSuccess) || (actorA.isLoading && actorB.isLoading)) {
    return <div>Chargement...</div>
  }

  return (
    <Provider store={store}>
      <div ref={gameRef} className={styles.game_container}>
        <div className={styles.canvas_container}>
          <canvas
            id="game_canvas"
          ></canvas>
        </div>
        <FreemanNode isActor={true} actorData={actorA.data?.data} />
      </div>
    </Provider>
  );
}

export default Game;