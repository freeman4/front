import { ChangeEvent, FunctionComponent, KeyboardEvent, useEffect, useRef, useState } from 'react';
import { v4 } from 'uuid';
import useDrawConnect from '../hooks/Node/useDrawConnect';
import useNotify from '../hooks/Node/useNotify';
import styles from '../styles/components/FreemanBloc.module.scss';
import { searchActors, searchMovies } from '../hooks/swrFetchers/searchTmdb';
import { useMutation } from 'react-query';
import { MovieCheckDto } from '../common/dto/tmbProxy/tmdb-proxy-movie.dto';
import { ActorCheckDto } from '../common/dto/tmbProxy/tmdb-proxy-actor.dto';

interface IProps {
  isActor: boolean,
  name: string,
  movieCheck?: MovieCheckDto,
  actorCheck?: ActorCheckDto,
  handleInput: (e: KeyboardEvent, text: string) => void,
  deleteNode?: () => void,
};

const FreemanBloc: FunctionComponent<IProps> = ({
  isActor,
  name,
  movieCheck,
  actorCheck,
  handleInput,
  deleteNode,
}) => {
  const [txt, setTxt] = useState('');
  const [listId, setListId] = useState('');
  const [error, setError] = useState(null);

  const ref = useRef<HTMLDivElement>(null);
  const movies = useMutation(searchMovies);
  const actors = useMutation(searchActors);
  const movieOptions = movies.data?.data ? movies.data.data.map((movie) => {
    return <option value={`${movie.title}`} key={v4()}>{`${movie.title} (${movie.release_date})`}</option>;
  }) : [];
  const actorOptions = actors.data?.data ? actors.data.data.map((actor) => {
    return <option value={`${actor.name}`} key={v4()} />;
  }) : [];
  const options = movieOptions.concat(actorOptions);

  const isN0 = deleteNode ? false : true;

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const inputTxt = e.target.value;
    if (inputTxt) {
      isActor ? movies.mutateAsync(inputTxt) : actors.mutateAsync(inputTxt);
      if (isActor && !movies.isLoading) movies.mutateAsync(inputTxt);
      if (!isActor && !actors.isLoading) actors.mutateAsync(inputTxt);
      setTxt(inputTxt);
    }
  }
  useDrawConnect(isN0, ref);
  useNotify(isN0);

  useEffect(() => {
    setListId(crypto.randomUUID());
  }, []);

  return (
    <div
      ref={ref}
      className={styles.freeman_bloc}
      tabIndex={0}
    >
      <div>
        <div>
          {name}
        </div>
        <input
          type="text"
          list={listId}
          onChange={handleChange}
          onKeyUp={(e) => handleInput(e, txt)}
        />
        <datalist id={listId}>
          {options}
        </datalist>
      </div>
      {
        deleteNode ?
          <input
            type="button"
            className={styles.del_button}
            onClick={deleteNode}
            value='-'
          />
          :
          <input
            type="button"
            disabled={true}
            className={`${styles.del_button}`}
            value='-'
          />
      }
    </div>
  );
}

export default FreemanBloc;