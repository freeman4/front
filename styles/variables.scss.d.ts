interface IGlobalScssVariables {
  nodeHeight: number;
  nodeGap: number;
  nodeGapVert: number;
}

export const nodeHeight: number;
export const nodeGap: number;
export const nodeGapVert: number;

const styles: IGlobalScssVariables;
export default styles;