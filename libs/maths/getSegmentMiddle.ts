import IPos from '../../models/maths/IPos';


/**
 * Calculates the middle of a segment thanks to its two points positioned in Oxy
 * @param fPos first position of a segment
 * @param sPos secong position of a segment
 * @returns the position of the middle
 */
const getSegmentMiddle = (fPos: IPos, sPos: IPos): IPos => {

  return {
    x: (sPos.x + fPos.x) / 2,
    y: (sPos.y + fPos.y) / 2, 
  };
}

export default getSegmentMiddle;