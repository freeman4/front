import IPos from '../../models/maths/IPos';
import getNorm from './getNorm';



/**
 * calculates the angle in radian of a position compared to another position 
 * which would be the center of a circle
 * @param center the position used as the circle center
 * @param pos the position to get radian
 * @returns an angle in radian between 0 and PI
 */
const getRadian = (center: IPos, pos: IPos): number => {
  const hypotenuse = getNorm(center, pos);
  const adjacent = Math.abs(pos.x - center.x);
  const cos = adjacent / hypotenuse;

  return Math.acos(cos);
}

export default getRadian;