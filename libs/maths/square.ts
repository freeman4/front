
const square = (x: number) => Math.pow(x, 2);

export default square;