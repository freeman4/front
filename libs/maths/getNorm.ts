import IPos from '../../models/maths/IPos';
import square from './square';


/**
 * Calculates the norm/length of a segment positioned in Oxy
 * @param fPos first position of the segment
 * @param sPos segond position of the segment
 * @returns the segment's norm
 */
const getNorm = (fPos: IPos, sPos: IPos): number => {

  return Math.sqrt(square(sPos.x - fPos.x) + square(sPos.y - fPos.y));
}

export default getNorm;