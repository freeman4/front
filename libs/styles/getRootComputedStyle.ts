
export enum VarType {
  Number = 'number',
}

const getRootComputedStyle = (varName: string, type: VarType) => {


  const cssVar = getComputedStyle(document.documentElement).getPropertyValue(varName);

  if (type === VarType.Number) return parseInt(cssVar);

  return cssVar;
}

export default getRootComputedStyle;