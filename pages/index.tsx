import type { NextPage } from 'next'
import Head from 'next/head'
import Image from 'next/image'
import { useRef } from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'
import Game from '../components/Game'
import useFullWindow from '../hooks/useFullWindow'
import styles from '../styles/Home.module.scss'

const Home: NextPage = () => {

  const firstDivRef = useRef<HTMLDivElement>(null);

  useFullWindow(firstDivRef);

  return (
    <div ref={firstDivRef}>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Freeman, a game for cinephiles" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <QueryClientProvider client={new QueryClient()}>
        <Game />
      </QueryClientProvider>
    </div>
  );
}

export default Home
